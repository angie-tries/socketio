window.Vue = new Vue({
  el: '#main',
  data: {
    socket: '',
    user: {
      name: '',
      msg: '',
      taken: false
    },
    msg: {
      to: '',
      from: '',
      content: ''
    },
    users: [],
    typers: [],
    nameForm: '',
    spaceInName: false,

    testMsgs: '<li class="self"><div class="message self"><b>sendername</b><p>test message content</p></div></li><li class="self"><div class="message self"><b>sendername</b><p>test message content</p></div></li><li class="self"><div class="message self"><b>sendername</b><p>test message content</p></div></li><li class="self"><div class="message self"><b>sendername</b><p>test message content</p></div></li>',

    chats: {
      'Chat Hall': {
        all: [],
        unread: []
      }
    },

    nsp: 'Chat Hall'
  },

  created () {
    this.socket = io()
    this.msg.from = this.socket.id
  },

  mounted () {
    this.socket.on('broadcast', msgObj => {
      this.createMsg(msgObj)
    })

    this.socket.on('typing', msgObj => {})
    this.socket.on('update users', users => this.users = users)
    this.socket.emit('disconnect', this.socket.id)
  },

  computed: {
    getChatName () {
      return this.nsp
    }
  },

  methods: {
    submitMsg () {
      if (this.user.msg.trim()) {
        this.socket.emit('broadcast', {
          id: this.socket.id,
          name: this.user.name,
          content: this.user.msg
        })
      }
      this.user.msg = ''
    },

    createMsg (msgObj) {
      const sender = document.createElement('b')
      sender.innerHTML = msgObj.name

      const msgBody = document.createElement('p')
      msgBody.innerHTML = msgObj.content

      const msgBubble = document.createElement('div')
      msgBubble.classList.add('message')

      const listItem = document.createElement('li')

      if (this.socket.id === msgObj.id) {
        msgBubble.classList.add('self')
        listItem.classList.add('self')
      }

      msgBubble.appendChild(sender)
      msgBubble.appendChild(msgBody)
      listItem.appendChild(msgBubble)
      const msgList = document.getElementById('msg-list')
      msgList.appendChild(listItem)
      console.log(listItem.innerHTML)
      msgList.scrollTop = msgList.scrollHeight
    },

    checkInput () {
      if (this.nameForm.trim().split('').includes(' ')) {
        return this.spaceInName = true
      } else {
        this.spaceInName = false
        this.assignName()
      }

    },

    assignName () {
      this.socket.emit('setName', {
        id: this.socket.id,
        name: this.nameForm
      }, success => {
        if (success) {
          this.user.name = this.nameForm
          this.user.taken = false

          // after assign, listen to its own id being called
          this.socket.on(this.nameForm, msg => {
            if (this.chats[msg.from]) {
              if (this.chats[msg.from].length) {

              }
            }
          })
        } else {
          this.user.taken = true
        }
      })
    },

    saveAndSwitch (user) {
      if (this.nsp === user.id) {

      }
      console.log(user)
    },

    showList (list) {
      const btns = document.querySelectorAll('.button')

      for (let i = 0; i < btns.length; i++) {
        if (btns[i].classList.contains('active')) {
          btns[i].classList.remove('active')
        }
      }

      const btn = document.getElementById(list + '-btn')
      btn.classList.add('active')

      const listing = document.getElementById('listings')
      listing.classList = ''
      listing.classList.add(list) // one class only
    },

    renderMsg (msg) {
      // function that preps and renders all the
      // msgs from js objs to lis
    }
  }
})
